import React from 'react';
import styled from 'styled-components';
import dadosIniciais from '../../data/dados_iniciais.json';
import BannerMain from '../../components/BannerMain';
import Carousel from '../../components/Carousel';
import Menu from '../../components/Menu';
import Footer from '../../components/Footer';

const Main = styled.main`
    background-color: var(--black);
    color: var(--white);
    flex: 1 1 0%;
    padding-top: 50px;
    padding-left: 5%;
    padding-right: 5%;
`;

function Home() {
  return (
    <>
      <Menu />
      <BannerMain
        videoTitle={dadosIniciais.categorias[0].videos[0].titulo}
        videoDescription="O que faz uma desenvolvedora front-end? #HipstersPontoTube"
        url={dadosIniciais.categorias[0].videos[0].url}
      />
      <Main>
        <Carousel
          ignoreFirstVideo
          category={dadosIniciais.categorias[0]}
        />

        <Carousel
          ignoreFirstVideo
          category={dadosIniciais.categorias[1]}
        />

        <Carousel
          ignoreFirstVideo
          category={dadosIniciais.categorias[2]}
        />

        <Carousel
          ignoreFirstVideo
          category={dadosIniciais.categorias[3]}
        />

        <Carousel
          ignoreFirstVideo
          category={dadosIniciais.categorias[4]}
        />

        <Carousel
          ignoreFirstVideo
          category={dadosIniciais.categorias[5]}
        />
      </Main>
      <Footer />
    </>
  );
}

export default Home;
