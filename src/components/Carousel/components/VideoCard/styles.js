import styled from 'styled-components';

export const VideoCardContainer = styled.a`
//border: 2px solid;
//border-radius: 4px;
text-decoration: none;
overflow: hidden;
cursor: pointer;
color: white;
width: 298px;
height: 197px;
background-image: ${({ url }) => `url(${url})`};
background-size: cover;
background-position: center;
position: relative;
display: flex;
align-items: flex-end;
padding: 16px;
transition: transform 100ms ease-out, border-radius 200ms ease-out;
transition-duration: .5s;

&:hover {
  transform: scale(1.5);
  z-index: 99999;
}

&:focus {
  opacity: .5;
}

&:not(:first-child) {
  margin-left: 20px;
}
`;
